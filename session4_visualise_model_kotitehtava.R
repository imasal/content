#' ---
#' title: "Kotitehtävä 4"
#' author: imasal
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session4_visualise_model_kotitehtava.R)


#+ setup
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=FALSE,cache=FALSE,warning=TRUE,message=TRUE))
library(tidyverse)

#' ## Kansiorakenteen luominen
#'
#' **Millä komennolla luot nykyisen työhakemistoon kansion `kotitehtava4`?**
#+ vastaus1
dir.create("kotitehtava4")


#' **Millä komennolla luot kansion 'kotitehtava4` alle kansion `kuviot`?**
#+ vastaus2
dir.create("./kotitehtava4/kuviot")

#'
#'
#' Datojen hakeminen
#'
#' Käytämme nyt taas dataa FAO:sla, joka löytyy osoiteesta:
#' '<http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip>
#' 
#' Data on valmiiksi prosessoitu seuraavalla skriptillä:
#' 
#'     download.file("http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip",
#'     destfile="../website/database/Production_Crops_E_All_Data_(Norm).zip")
#'     unzip("./Production_Crops_E_All_Data_(Norm).zip")
#'     d <- readr::read_csv("../website/database/Production_Crops_E_All_Data_(Norm).csv")
#'     saveRDS(d, "../website/database/Production_Crops_E_All_Data_(Norm).RDS")
#'     
#'  Data on ladattavissa R:ään komennolla `fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))`
#'
#'  Lataa data ja printtaa montako erillistä maata datassa on
#+ vastaus3
n_distinct(fao$Country)


#' *Jatkossa tehväsissä oletetaan että objekti `fao`**
#' 
#' 
#' **Kuinka monta tonnia Suomessa tuotettiin tomaatteja vuonna 1990-2012**?
#' **Tee uudesta datasta objekti `tomatoes`*
#+ vastaus4
fao %>%
  filter(Country == "Finland", Item == "Tomatoes", Year %in% 1990:2012) -> tomatoes

#' Luennolla käytiin läpi että ggplot2-paketilla piirretään viivakuvio syntaksiilla
#' `ggplot(data=d, aes(x=xmuuttuja,y=ymuuttuja)) + geom_line()`
#' 
#' **Piirrä datasta `tomatoes` viivakuvio jossa x-akselilla on vuodet ja 
#' y-akselilla tomaattien määrä**
#'
#+ vastaus5
tomatoes %>%
  filter(Element == 'Production') %>%
  ggplot(aes(x=Year, y = Value)) + geom_line()



#' Luo aikaisemman tehtävän tapaan data ja ota siihen tomaattien ohella myös 
#' ruis, vehnä ja kaura ja anna nimeksi `viljat`. 
#' 
#' Piirrä datasta samanmoinen viivakuvio, 
#' jossa eri viljat on eri värisellä viivalla
#' 
#+ vastaus6
fao %>%
  filter(Country == "Finland", Item %in% c("Tomatoes", "Rye", "Wheat", "Oats")) -> viljat
viljat %>%
  filter(Element == "Production") %>%
  ggplot(aes(x=Year, y=Value, color = Item)) + geom_line()

#' Tee samoista viljoista toinen kuvio, jossa tuotannon sijaan tarkastellaan
#' viljelyalaa ja että yksikkö on tonnien sijaan hehtaari.
#' 
#+ vastaus7
viljat %>%
  filter(Element == 'Area harvested') %>%
  ggplot(aes(x=Year, y=Value, color = Item)) + geom_line()

ggplot(data = subset(viljat, Element == "Area harvested"), aes(x=Year, y=Value, color = Item)) + geom_line()
#' Tehdään sitten tolppakuvioita. Valitse edellisen esimerkin kasvit ja viljelyala, 
#' mutta valitse vuodeksi 2012. Piirrä tolppakuvio geom_bar()-funktiolla 
#' (muista `stat="identity"` parametri!
#+ vastaus8
viljat %>%
  filter(Element == "Area harvested", Year == 2012) %>%
  ggplot(aes(x = Item, y=Value, fill = Item)) + geom_bar(stat='identity')



#' Pudota tomaatit ja ruis pois tarkastelusta ja tarkastellaan sadon määrää (Yeild) yksikkönä
#' hehtogrammaa hehtaarilta. Ota mukaan myös maat joilla on Suomen kanssa maaraja ja piirrä sama tolppakuvio mutta siten että maat ovat omia paneelejaan (`facet_wrap(~var)`)
#' 
#+ vastaus9
fao %>%
  filter(Country %in% c('Finland', 'Sweden', 'Norway', 'Russian Federation'), Year == 2012, Item %in% c("Wheat", "Oats"),Element == "Yield") %>%
  ggplot(aes(x=Item, y=Value, fill = Item)) + geom_bar(stat = 'identity') + facet_wrap( ~Country)  


#' Pudotetaan Venäjä pois ja tarkastellaan Suomen, Ruotsin, Norjan ja Tanskan
#' kesken vehnän tuotannon osalta sitä, miten 
#' tuotannon määrä (tonneja) ja viljelysala ovat yhteydessä toisiinsa piirtämällä hajontakuvio
#' jossa x-akselilla on tuotanto ja y-akselilla viljelysala. Kenties helpoin tapa on tehdä kaksi dataa
#' ja yhdistää ne left join. Saatat joutua vaihtamaan Value-muuttujan nimeä.
#+ vastaus10
fao %>%
  select(-Unit, -Flag) %>%
  filter(Country %in% c('Finland', 'Sweden', 'Norway', 'Denmark'), Year == 2012,
         Item == "Wheat",Element %in% c('Production',"Yield", 'Area harvested')) ->pohjanvilja
pohjanvilja %>% filter(Element == "Production") %>% mutate(Production = Value) %>%  select(-Value, -Element)-> pohjanvilja_P
pohjanvilja %>% filter(Element == 'Yield') %>% mutate(Yield = Value) %>%  select(-Value, -Element) -> pohjanvilja_Y
left_join(pohjanvilja_P, pohjanvilja_Y, by=c('Country', 'Year')) -> pohjanvilja_PY
ggplot(pohjanvilja_PY, aes(x=Production, y=Yield, color = Country)) + geom_point() 

#' Jatketaan samoilla spekseillä mutta otetaan vuodet 1990-2012 ja piirretään maittaiset viivakuviot
#' niin että paneeleissa on tuotanto ja viljelysala 
#' 
#+ vastaus11
pohjanvilja %>% filter(Element == 'Area harvested') %>% mutate(Area_harvested = Value) %>%  select(-Value, -Element) -> pohjanvilja_A
left_join(pohjanvilja_P, pohjanvilja_A, by=c('Country', 'Year')) -> pohjanvilja_PA
pohjanvilja %>%
  filter(Year %in% 1990:2012, Element %in% c("Production", 'Area harvested')) %>%
  ggplot(aes(x=Year, y = Value, color = Country)) + geom_line() + facet_wrap(~Element)




