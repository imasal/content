#' ---
#' title: "Kotitehtävä 4"
#' author: utu_tunnus
#' output:
#'   html_document:
#'  #   toc: true
#'  #   toc_float: true
#'     number_sections: yes
#'     code_folding: show
#' ---

#' [Linkki kotitehtävän lähdekoodiin gitlab:ssa](https://gitlab.com/utur2016/content/raw/master/session4_visualise_model_kotitehtava.R)


#+ setup
library(knitr)
opts_chunk$set(list(echo=TRUE,eval=TRUE,cache=FALSE,warning=TRUE,message=TRUE))
library(tidyverse)

#' ## Kansiorakenteen luominen
#'
#' **Millä komennolla luot nykyisen työhakemistoon kansion `kotitehtava4`?**
#+ vastaus1
dir.create("./kotitehtava4")

#' **Millä komennolla luot kansion 'kotitehtava4` alle kansion `kuviot`?**
#+ vastaus2
dir.create("./kotitehtava4/kuviot")

#'
#'
#' Datojen hakeminen
#'
#' Käytämme nyt taas dataa FAO:sla, joka löytyy osoiteesta:
#' '<http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip>
#'
#' Data on valmiiksi prosessoitu seuraavalla skriptillä:
#'
#'     download.file("http://faostat3.fao.org/faostat-bulkdownloads/Production_Crops_E_All_Data_(Norm).zip",
#'     destfile="../website/database/Production_Crops_E_All_Data_(Norm).zip")
#'     unzip("./Production_Crops_E_All_Data_(Norm).zip")
#'     d <- readr::read_csv("../website/database/Production_Crops_E_All_Data_(Norm).csv")
#'     saveRDS(d, "../website/database/Production_Crops_E_All_Data_(Norm).RDS")
#'
#'  Data on ladattavissa R:ään komennolla `fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))`
#'
#'  Lataa data ja printtaa montako erillistä maata datassa on
#+ vastaus3
fao <- readRDS(gzcon(url("http://courses.markuskainu.fi/utur2016/database/Production_Crops_E_All_Data_(Norm).RDS")))
# fao %>% summarise(n = n_distinct(Country))


#' *Jatkossa tehväsissä oletetaan että objekti `fao`**
#'
#'
#' **Kuinka monta tonnia Suomessa tuotettiin tomaatteja vuonna 1990-2012**?
#' **Tee uudesta datasta objekti `tomatoes`*
#+ vastaus4
d <- fao
table(d$Country)
table(d$Element)
table(d$Item)
table(d$Unit)
table(d$Year)
d %>% filter(Country %in% "Finland",
             Element %in% "Production",
             Item %in% "Tomatoes",
             Unit %in% "tonnes",
             Year %in% 1990:2012
) %>%
  select(Year,Value) -> tomatoes

#' Luennolla käytiin läpi että ggplot2-paketilla piirretään viivakuvio syntaksiilla
#' `ggplot(data=d, aes(x=xmuuttuja,y=ymuuttuja)) + geom_line()`
#'
#' **Piirrä datasta `tomatoes` viivakuvio jossa x-akselilla on vuodet ja
#' y-akselilla tomaattien määrä**
#'
#+ vastaus5
ggplot(tomatoes, aes(x=Year,y=Value)) + geom_line()


#' Luo aikaisemman tehtävän tapaan data ja ota siihen tomaattien ohella myös
#' ruis, vehnä ja kaura ja anna nimeksi `viljat`.
#'
#' Piirrä datasta samanmoinen viivakuvio,
#' jossa eri viljat on eri värisellä viivalla
#'
#+ vastaus6
d %>% filter(Country %in% "Finland",
             Element %in% "Production",
             Item %in% c("Tomatoes","Rye","Wheat","Oats"),
             Unit %in% "tonnes",
             Year %in% 1990:2012
) %>%
  select(Year,Value,Item) -> viljat

ggplot(viljat, aes(x=Year,y=Value,color=Item)) + geom_line()


#' Tee samoista viljoista toinen kuvio, jossa tuotannon sijaan tarkastellaan
#' viljelyalaa ja että yksikkö on tonnien sijaan hehtaari.
#'
#+ vastaus7
d %>% filter(Country %in% "Finland",
             Element %in% "Area harvested",
             Item %in% c("Tomatoes","Rye","Wheat","Oats"),
             Unit %in% "Ha",
             Year %in% 1990:2012
) %>%
  select(Year,Value,Item) -> viljat

ggplot(viljat, aes(x=Year,y=Value,color=Item)) + geom_line()


#' Tehdään sitten tolppakuvioita. Valitse edellisen esimerkin kasvit ja viljelyala,
#' mutta valitse vuodeksi 2012. Piirrä tolppakuvio geom_bar()-funktiolla
#' (muista `stat="identity"` parametri!
#+ vastaus8
d %>% filter(Country %in% "Finland",
             Element %in% "Area harvested",
             Item %in% c("Tomatoes","Rye","Wheat","Oats"),
             Unit %in% "Ha",
             Year %in% 2012
) %>%
  select(Year,Value,Item) -> viljat

ggplot(viljat, aes(x=Item,y=Value)) + geom_bar(stat="identity")



#' Pudota tomaatit ja ruis pois tarkastelusta ja tarkastellaan sadon määrää (Yeild) yksikkönä
#' hehtogrammaa hehtaarilta. Ota mukaan myös maat joilla on Suomen kanssa maaraja ja piirrä sama tolppakuvio mutta siten että maat ovat omia paneelejaan (`facet_wrap(~var)`)
#'
#+ vastaus9
d %>% filter(Country %in% c("Finland","Sweden","Norway","Russian Federation"),
             Element %in% "Yield",
             Item %in% c("Wheat","Oats"),
             Unit %in% "Hg/Ha",
             Year %in% 2012
) %>%
  select(Year,Value,Item,Country) -> viljat

ggplot(viljat, aes(x=Item,y=Value)) + geom_bar(stat="identity") +
  facet_wrap(~Country)


#' Pudotetaan Venäjä pois ja tarkastellaan Suomen, Ruotsin, Norjan ja Tanskan
#' kesken vehnän tuotannon osalta sitä, miten
#' tuotannon määrä (tonneja) ja viljelysala ovat yhteydessä toisiinsa piirtämällä hajontakuvio
#' jossa x-akselilla on tuotanto ja y-akselilla viljelysala. Kenties helpoin tapa on tehdä kaksi dataa
#' ja yhdistää ne left join. Saatat joutua vaihtamaan Value-muuttujan nimeä.
#+ vastaus10
d %>% filter(Country %in% c("Finland","Sweden","Norway","Denmark"),
             Element %in% "Production",
             Item %in% "Wheat",
             Unit %in% "tonnes",
             Year %in% 2012
) %>%
  select(Country,Value) %>%
  rename(tuotanto = Value) -> tuotanto

d %>% filter(Country %in% c("Finland","Sweden","Norway","Denmark"),
             Element %in% "Area harvested",
             Item %in% "Wheat",
             Unit %in% "Ha",
             Year %in% 2012
) %>%
  select(Country,Value) %>%
  rename(viljelysala = Value) -> viljelysala

dat <- left_join(tuotanto,viljelysala)
ggplot(dat, aes(x=tuotanto,y=viljelysala,color=Country)) + geom_point()


#' Jatketaan samoilla spekseillä mutta otetaan vuodet 1990-2012 ja piirretään maittaiset viivakuviot
#' niin että paneeleissa on tuotanto ja viljelysala (koska tuotanto ja viljelysalan luvut ovat hyvin erisuuret, 
#' voit vapauttaa paneeleiden y-skaalan lisäämällä `facet_wrap()`-funtioon parametrin `scales="free_y`)
#'
#+ vastaus11
d %>% filter(Country %in% c("Finland","Sweden","Norway","Denmark"),
             Element %in% "Production",
             Item %in% "Wheat",
             Unit %in% "tonnes",
             Year %in% 1990:2012
) %>%
  select(Country,Year,Value) %>%
  mutate(type = "tuotanto") -> tuotanto

d %>% filter(Country %in% c("Finland","Sweden","Norway","Denmark"),
             Element %in% "Area harvested",
             Item %in% "Wheat",
             Unit %in% "Ha",
             Year %in% 1990:2012
) %>%
  select(Country,Year,Value) %>%
  mutate(type = "viljelysala") -> viljelysala

dat <- bind_rows(tuotanto,viljelysala)
ggplot(dat, aes(x=Year,y=Value,color=Country)) + geom_line() + facet_wrap(~type, scale="free_y")
